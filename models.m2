MODULE models;
 TYPE
 	Comment = RECORD
		PostID: INTEGER;
		Name: ARRAY 25 OF CHAR;
		Email: ARRAY 25 OF CHAR;
		Comment: ARRAY 2000 OF CHAR;
		Parent: INTEGER;
	END;
END models.
